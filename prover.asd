(defsystem prover
  :author "Hirotetsu Hongo <ekatsymeee@gmail.com>"
  :license "MIT"
  :depends-on (:cl-algebraic-data-type
               :alexandria)
  :components ((:module "src"
                :serial t
                :components
                ((:file "util")
                 (:file "core")
                 (:file "axiom")
                 (:file "package"))
                ))
  )

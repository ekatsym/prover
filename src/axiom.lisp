(defpackage prover.axiom
  (:use :cl
        :prover.util
        :prover.core
        )
  (:export)
  )

(in-package :prover.axiom)


;;; cons axioms
(defaxiom atom/cons (x y)
  (equal (atom (cons x y)) nil))

(defaxiom car/cons (x y)
  (equal (car (cons x y)) x))

(defaxiom cdr/cons (x y)
  (equal (cdr (cons x y)) y))

(defaxiom cons/car+cdr (x)
  (if (atom x)
      t
      (equal (cons (car x) (cdr x)) x)))


;;; equal axioms
(defaxiom equal-same (x)
  (equal (equal x x) t))

(defaxiom equal-swap (x y)
  (equal (equal x y) (equal y x)))

(defaxiom equal-if (x y)
  (if (equal x y) (equal x y) t))

;;; if axioms
(defaxiom if-true (x y)
  (equal (if t x y) x))

(defaxiom if-false (x y)
  (equal (if nil x y) y))

(defaxiom if-same (x y)
  (equal (if x y y) y))

(defaxiom jabberwocky (x)
  (if (brilling x)
      (if (slithy x)
          (equal (mimsy x) 'borogove)
          (equal (mome x) 'rath))
      (if (uffish x)
          (equal (frumious x) 'bandersnatch)
          (equal (frabjous x) 'beamish))))

;;; bug test
#|
(rewrite (if (atom (car a))
             (if (equal (car a) (cdr a))
                 'hominy
                 'grits)
             (if (equal (cdr (car a)) '(hash browns))
                 (cons 'ketchup (car a))
                 (cons 'mustard (car a))))
  (cons/car+cdr ((car a)) (car a)))
|#

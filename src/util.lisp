(defpackage prover.util
  (:use :cl)
  (:import-from :adt
                #:defdata
                #:match
                )
  (:import-from :alexandria
                #:with-gensyms
                #:once-only
                )
  (:export #:defdata
           #:match
           #:with-gensyms
           #:once-only
           #:force-list
           #:alist-cons
           #:pattern-match
           )
  )

(in-package :prover.util)

(defun force-list (object)
  (if (listp object)
      object
      (list object)))

(defun alist-cons (key val alist)
  (cons (cons key val) alist))

(defun pattern-match (tree1 tree2)
  (labels ((rec (t1 t2)
             (cond ((equal t1 t2) nil)
                   ((or (atom t1) (atom t2)) (list (cons t1 t2)))
                   (t (let ((hd1 (car t1)) (tl1 (cdr t1))
                            (hd2 (car t2)) (tl2 (cdr t2)))
                        (nconc (pattern-match hd1 hd2)
                               (pattern-match tl1 tl2)))))))
    (remove-duplicates (rec tree1 tree2) :test #'equal)))


(defpackage prover.predicate
  (:use :cl
        :prover.util
        )
  (:export #:equal
           #:if
           )
  )

(in-package :prover.predicate)


#|
$predicate list
* %and
* %or
* %not
* %->
* %<->

(defparameter *logical-terms*
  (list 'imply 'and 'or 'not 'forall 'exists))

(and (exists x
             (p a b x))
     (exists x
             (not (exists w
                          (or (q w x)
                                  (forall x
                                          (p x w)))))))

(defstruct (prop (:constructor -> (hyp con))
                        (:print-object (lambda (prop stream)
                                         (if (eq (prop-hyp prop) t)
                                             (format stream "#PROP[~a]"
                                                     (prop-con prop))
                                             (format stream "#PROP[~a -> ~a]"
                                                     (prop-hyp prop)
                                                     (prop-con prop))))))
  (hyp hyp :type (or list symbol))
  (con con :type (or list symbol)))

(defun hyps-add-and (hyps)
  (cond ((null hyps) t)
        ((= 1 (length hyps)) (car hyps))
        (t `(and ,@hyps))))

(defun prop (expr &optional hyps)
  (let ((pred (if (atom expr) expr (car expr))))
    (case pred
      (equal (-> (hyps-add-and hyps) expr))
      (if (destructuring-bind (test then else) (cdr expr)
            (-> (hyps-add-and hyps)
                `(and ,(pred->prop then `(,test ,@hyps))
                      ,(pred->prop else `((not ,test) ,@hyps))))))
      )
    )
  )

(pred->prop
  '(equal (equal x x) t))

(pred->prop ; #PROP[(and #PROP[(equal x y) -> (equal x y)] #PROP[(not (equal x y)) -> (not (equal x y))])]
  '(if (equal x y)
       (equal x y)
       t))


|#

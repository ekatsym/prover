(defpackage prover.core
  (:use :cl
        :prover.util
        )
  (:export #:prop
           #:proved-p
           #:defaxiom
           #:deftheorem
           #:rewrite
           #:unproved-error
           )
  )

#|
PROVER.COREは主にPROPのためのパッケージで，
より詳しく言えば，PROPとPROPを証明するための道具を提供します．
PROPは論理学における命題を意識していますが，
Common Lispで扱いやすいよう一部独自の解釈がなされているかもしれません．

命題は「変数」，「仮定」，「帰結」の3要素から成るとしています．
「帰結」とは"(equal x y)"というリストのことです．
「仮定」とはリストであり，「仮定」が満たされた環境では「帰結」を用いたリストの書き換えが可能です．
「変数」とは"(equal x y)"における"x"や"y"のことです．

構造体PROP-COREは
* VARS
* HYPOTHESISES
* CONSEQUENCE
* PROVED-P
というスロットをもち，それぞれ
* 変数のリスト
* 仮定のリスト
* 帰結
* 証明判定
を意味します．
"VARS"を"(x y)"，"HYPOTHESISES"を"(H1 H2 H3)"，"CONSEQUENCE"を"(equal (f x) (g y))"とすると，
PROP-COREは"forall x y, H1(x,y) ^ H2(x,y) ^ H3(x,y) -> (f(x) <-> g(y))"という論理式を意味します．
PROP-COREは「任意の状態の変数に対し，仮定を全て満たすとき，2式は等価である」という主張のみが可能です．
PROP-COREは1つの帰結のみをもちます．

構造体PROPはPROP-COREを拡張したもので，
* VARS
* PROPS
というスロットをから成ります．
PROPSはPROP-COREのリストです．
PROPSは複数のPROP-COREをもつことで，複数の帰結をもち，仮定においてORに等価な表現が可能です．
たとえば，
* "forall x y, H1(x,y) v (H2(x,y) ^ (H3(x,y)) -> (f(x) <-> g(y))"
という論理式は
* "forall x y, H1(x,y) -> (f(x) <-> g(y))"
を表すPROP-COREと
* "forall x y, H2(x,y) ^ H3(x,y) -> (f(x) <-> g(y))"
を表すPROP-COREのリストで表します．
|#


(in-package prover.core)

(defstruct (prop-core (:constructor prop-core (vars hypothesises consequence))
                 (:print-object
                   (lambda (prop-core stream)
                     (format stream
                             (concatenate 'string
                                          "("
                                          (if (null (prop-core-hypothesises prop-core))
                                              ""
                                              (format nil
                                                      "~{~a~^ ^ ~} -> "
                                                      (prop-core-hypothesises prop-core)))
                                          (format nil "~a" (prop-core-consequence prop-core))
                                          (if (prop-core-proved-p prop-core)
                                              " : proved"
                                              " : not proved")
                                          ")")))))
  "PROP-CORE is prop which has a only one consequence."
  (vars vars)
  (hypothesises hypothesises)
  (consequence consequence)
  (proved-p nil))

(define-condition malformed-prop-error (error)
  ((datum :initarg :datum
          :accessor malformed-prop-error-datum)
   (illegal-part :initarg :illegal-part
                 :accessor malformed-prop-error-illegal-part))
  (:report (lambda (condition stream)
             (format stream "~&A expression~%~a~%cannot be converted into PROP.~%An illegal part is ~%~a."
                     (malformed-prop-error-datum condition)
                     (malformed-prop-error-illegal-part condition)))))

(defun expr->prop-cores (vars hyps body)
  (if (listp body)
      (case (car body)
        (equal (list (prop-core vars hyps body)))
        (if (destructuring-bind (test then &optional else) (cdr body)
              (append
                (handler-case (expr->prop-cores vars `(,@hyps ,test) then)
                  (malformed-prop-error () nil))
                (when else
                  (handler-case (expr->prop-cores vars `(,@hyps (not ,test)) else)
                    (malformed-prop-error () nil))))))
        (otherwise (error 'malformed-prop-error :datum body :illegal-part (car body))))
      (error 'malformed-prop-error :datum body :illegal-part body)))

(defstruct (prop (:constructor prop (vars hypothesises body))
                 (:print-object
                   (lambda (prop stream)
                     (let ((*print-circle* nil))
                       (format stream
                               "#PROP(~a~{~a~^~%~6t~} : ~a)"
                               (if (null (prop-vars prop))
                                   ""
                                   (format nil "forall ~{~a~^ ~},~%~6t"
                                           (prop-vars prop)))
                               (prop-props prop)
                               (if (every #'prop-core-proved-p (prop-props prop))
                                   "proved"
                                   "not proved"))))))
  (vars vars)
  (props (expr->prop-cores vars hypothesises body)))

(defun proved-p (prop)
  (every #'prop-core-proved-p (prop-props prop)))

(define-condition focus-unmatch-error (error)
  ((datum :initarg :datum
          :reader focus-unmatch-error-datum)
   (expected-focus :initarg :expected-focus
                   :reader focus-unmatch-error-expected-focus))
  (:report (lambda (condition stream)
             (format stream "~&A~%~a~%doesn't include ~%~a."
                     (focus-unmatch-error-datum condition)
                     (focus-unmatch-error-expected-focus condition)))))

(defun focus-hypss (expr focus &optional hyps)
  (labels ((rec (exr hps)
             (cond ((equal focus exr) (list hps))
                   ((atom exr) (list (list 'unmatch)))
                   ((eq 'if (car exr))
                    (destructuring-bind (test then &optional else) (cdr exr)
                      (append
                        (rec then `(,@hps ,test))
                        (when else
                          (rec else `(,@hps (not ,test)))))))
                   (t (mapcan (lambda (arg) (rec arg hps))
                              (cdr exr))))))
    (let* ((hypss-with-unmatch (rec expr hyps))
           (hypss (remove-if (lambda (hyps) (eq (car hyps) 'unmatch))
                             hypss-with-unmatch)))
      (or hypss (error 'focus-unmatch-error :datum expr :expected-focus focus)))))

(defun prop-hyps->cons (args hyps prop)
  (let* ((props (prop-props prop))
         (var-binds (mapcar (lambda (prop-var arg) (cons prop-var arg))
                            (prop-vars prop) args)))
    (labels ((rec (ps acc)
               (if (null ps)
                   acc
                   (let ((head (car ps))
                         (tail (cdr ps)))
                     (rec tail (if (null (set-difference (sublis var-binds (prop-core-hypothesises head)
                                                                 :test #'equal)
                                                         hyps
                                                         :test #'equal))
                                   (cons (sublis var-binds (prop-core-consequence head)
                                                 :test #'equal) acc)
                                   acc))))))
      (rec props nil))))

(defun prop-hypss->cons (args hypss prop)
  (mapcan (lambda (hyps) (prop-hyps->cons args hyps prop))
          hypss))

(define-condition unproved-error (error)
  ((prop :initarg :prop
         :reader unproved-error-prop))
  (:report (lambda (condition stream)
             (format stream "The Proposition~%~a~%is not proved."
                     (unproved-error-prop condition)))))

(defun apply-prop (prop args expr focus &optional allow-unproved-prop-p)
  (if (or allow-unproved-prop-p (proved-p prop))
      (let ((cns (prop-hypss->cons
                   args
                   (focus-hypss expr focus)
                   prop)))
        (labels ((new-focus (cs)
                   (if (null cs)
                       focus
                       (destructuring-bind (equal x y) (car cs)
                         (declare (ignore equal))
                         (cond ((equal x focus) y)
                               ((equal y focus) x)
                               (t (new-focus (cdr cs))))))))
          (subst (new-focus cns) focus expr
                 :test #'equal)))
      (error 'unproved-error :prop prop)))

(defmacro deftheorem (name vars &body body)
  `(defparameter ,name
     (prop ',vars '() ',(car body))))

(defmacro defaxiom (name vars &body body)
  (with-gensyms (axiom)
    `(defparameter ,name
       (let ((,axiom (prop ',vars '() ',(car body))))
         (mapc (lambda (p) (setf (prop-core-proved-p p) t))
               (prop-props ,axiom))
         ,axiom))))

(defmacro rewrite (expression &body steps)
  (if (null steps)
      `(quote ,expression)
      `(rewrite ,(eval (destructuring-bind (prop args focus &optional allow-unproved-prop-p) (car steps)
                         `(apply-prop ,prop ',args ',expression ',focus ,allow-unproved-prop-p)))
         ,@(cdr steps))))

;;; test
#|
(defaxiom jabberwcky (x)
  (if (brilling x)
      (if (slithy x)
          (equal (mimsy x) 'borogove)
          (equal (mome x) 'rath))
      (if (uffish x)
          (equal (frumious x) 'bandersnatch)
          (equal (frabjous x) 'beamish))))

(apply-prop jabberwcky
            `('(callooh callay))
            '(cons 'gyre
                   (if (uffish '(callooh callay))
                       (cons 'gimble
                             (if (brilling '(callooh callay))
                                 (cons 'borogove '(outgrabe))
                                 (cons 'bandersnatch '(wabe))))
                       (cons (frabjous '(callooh callay)) '(vorpal))))
            ''bandersnatch)

(rewrite (cons 'gyre
               (if (uffish '(callooh callay))
                   (cons 'gimble
                         (if (brilling '(callooh callay))
                             (cons 'borogove '(outgrabe))
                             (cons 'bandersnatch '(wabe))))
                   (cons (frabjous '(callooh callay)) '(vorpal))))
  (jabberwcky ('(callooh callay)) 'bandersnatch)
  (jabberwcky ('(callooh callay)) (frumious '(callooh callay))))
|#

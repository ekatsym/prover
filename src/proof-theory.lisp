(defpackage prover.proof-theory
  (:use :cl
        :prover.util)
  (:export)
  )

(in-package :prover.proof-theory)

(defparameter *logical-operator*
  '(-> and or not forall exists))

(defun logical-expression-p (object)
  (or (symbolp object)
      (and (listp object)
           (member (car object) *logical-operator* :test #'eq)
           (every #'logical-expression-p
                  (cdr object)))))

(defun proof-tree (hypothesises derivations)
  (when (and (every #'logical-expression-p hypothesises)
             (every #'logical-expression-p derivations))
    (cons hypothesises derivations)))

(defun proof-tree-p (object)
  (and (listp object)
       (and (logical-expression-p (car object))
            (every #'logical-expression-p (cdr object)))))

(defun prem (proof-tree expr &optional scope new-derivation-p)
  (let ((scope (or scope proof-tree)))
    (if (not new-derivation-p)
        (let ((hyps (car scope))
              (ders (cdr scope)))
          (subst (cons `(,@hyps ,expr)
                       ders)
                 scope
                 proof-tree
                 :test #'equal))
        (let ((hyps (car scope))
              (ders (cdr scope)))
          (subst (cons hyps
                       `(,@ders ,(proof-tree (list expr) nil)))
                 scope
                 proof-tree
                 :test #'equal)))))

(defun hyps-in-scope (proof-tree scope)
  (let ((hyps (car proof-tree))
        (ders (cdr proof-tree)))
    (cond ((equal proof-tree scope) (append hyps ders))
          ((not (some #'proof-tree-p ders)) nil)
          (t (append hyps
                     ders
                     (mapcan (lambda (pt) (hyps-in-scope pt scope))
                             (remove-if-not #'proof-tree-p ders)))))))

(defun rep (proof-tree expr &optional scope)
  (let* ((scope (or scope proof-tree))
         (hyps (car scope))
         (ders (cdr scope)))
    (if (member expr hyps :test #'equal)
        (subst (cons hyps
                     `(,@ders ,expr))
               scope
               proof-tree
               :test #'equal)
        proof-tree)))

(defun reit (proof-tree expr &optional scope)
  (let ((scope (or scope proof-tree)))
    (if (member expr (hyps-in-scope proof-tree scope) :test #'equal)
        (subst (let ((hyps (car scope))
                     (ders (cdr scope)))
                 (cons hyps
                       `(,@ders ,expr)))
               scope
               proof-tree
               :test #'equal)
        proof-tree)))

(defun ->elim (proof-tree ->expr prop &optional scope)
  (let* ((scope (or scope proof-tree))
         (hyps (car scope))
         (ders (cdr scope)))
    (if (and (member ->expr ders :test #'equal)
             (member prop ders :test #'equal))
        (subst (cons hyps
                     `(,@ders ,(third ->expr)))
               scope
               proof-tree
               :test #'equal)
        proof-tree)))

(defun ->intro (proof-tree scope))

(defmacro proof (proof-tree-name initial-hypothesises initial-derivations &body body)
  (declare (ignore proof-tree-name))
  (reduce (lambda (accumulate manipulation)
            `(,(car manipulation) ,accumulate ,@(mapcar (lambda (x) `(quote ,x))
                                                        (cddr manipulation))))
          body
          :initial-value `(proof-tree ',initial-hypothesises ',initial-derivations)))

(proof pt ((-> p q)) ()
  (prem pt p)
  (reit pt (-> p q))
  (reit pt p)
  (->elim pt (-> p q) p)
  )

(proof pt ((-> p q)) (s)
  (prem pt (-> q r))
  (prem pt p)
  (prem pt (-> r s) nil t)
  (rep pt (-> p q))
  (reit pt (-> p q) (((-> r s))))
  )

(proof pt ((-> p q)) (s)
  (prem pt (-> q r))
  (prem pt p)
  (prem pt (-> r s)))

(prem (prem (prem '(-> p q))))


(let ((pt (proof-tree nil)))
  (prem pt '(-> p q))
  (prem pt '(and (not q) r))
  (prem pt '(-> s (not p)))

  (prem '(-> s (not p))
        (prem '(and (not q) r)
              (prem '(-> p q) pt))))

;;; natural-deduction

(defun proof-tree-p (object)
  (and (listp cons)
       (listp (car cons))
       (every (lambda (hyp) ()))
       )
  )

(defun prem (proof-tree scope &rest exprs)
  (let* ((scope (or scope proof-tree))
         (hyp (car scope))
         (derivation (cdr scope))
         (new-scope `((,@hyp ,@exprs) ,@derivation)))
    (subst new-scope scope proof-tree :test #'equal)))

(defun rep (proof-tree scope &rest exprs)
  (let* ((scope (or scope proof-tree))
         (hyp (car scope))
         (derivation (cdr scope))
         (new-scope `((,@hyp)
                      ,@derivation
                      ,@(remove-if-not (lambda (expr) (member expr hyp :test #'equal))
                                       exprs))))
    (subst new-scope scope proof-tree :test #'equal)))

(defun depend-derivation (proof-tree scope)
  (labels ((rec (tree acc)
             (cond ((null tree) acc)
                   ((proof-tree-p (car tree))
                    (rec (cdar tree) )
                    )
                   )
             ))
    (rec (cdr proof-tree) nil)
    )
  )

(defun reit (proof-tree scope &rest exprs)
  (let* ((scope (or scope proof-tree))
         (hyp (car scope))
         (derivation (cdr scope))
         (new-scope `((,@hyp)
                      ,@derivation
                      ,@(remove-if-not
                          (lambda (expr)
                            (member expr (flatten (butmember scope )))))
                      ))
         ))
  )

(prem (proof-tree (list '(-> p q)) (list 'r))
      nil
      '(-> q r)
      )

(rep (prem (proof-tree (list '(-> p q)) (list 'r))
           (proof-tree (list '(-> p q)) (list 'r))
           '(-> q r))
     nil
     'q
     '(-> p q)
     )

(rep (prem '(()) '(())))

(prem '(((-> p q))
        p
        q)
      '(p q)
      'r)

(defun rep (proof-tree &rest exprs)
  (destructuring-bind (hyp &rest body) proof-tree
    `((,@hyp)
      ,@body
      ,@exprs)))

(prem (prem `(()) '(and p q)) '(or p q))
